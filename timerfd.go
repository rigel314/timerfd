package timerfd

import (
	"errors"
	"golang.org/x/sys/unix"
	"syscall"
	"time"
	"unsafe"
)

type Timer struct {
	fd uintptr
}

type itimerspec struct {
	Interval unix.Timespec
	Value    unix.Timespec
}

// Return nil on failure
func Tick(interval time.Duration) <-chan time.Time {
	tim, err := Create(interval)
	if err != nil {
		return nil
	}

	ch := make(chan time.Time)
	go func(ch chan<- time.Time) {
		for {
			tim.blockedRead()
			ch <- time.Now()
		}
	}(ch)

	return ch
}

func Create(interval time.Duration) (*Timer, error) {
	fd, _, err := syscall.Syscall(unix.SYS_TIMERFD_CREATE, 1, 0, 0)
	if err != 0 {
		return nil, err
	}
	if fd < 0 {
		return nil, errors.New("bad return from timerfd_create()")
	}
	tv := itimerspec{Interval: unix.NsecToTimespec(interval.Nanoseconds()), Value: unix.NsecToTimespec(interval.Nanoseconds())}
	ret, _, err := syscall.Syscall6(unix.SYS_TIMERFD_SETTIME, fd, 0, uintptr(unsafe.Pointer(&tv)), 0, 0, 0)
	if err != 0 {
		return nil, err
	}
	if ret != 0 {
		return nil, errors.New("bad return from timerfd_settime()")
	}
	return &Timer{fd: fd}, nil
}

func (t *Timer) Close() error {
	ret, _, err := syscall.Syscall(unix.SYS_CLOSE, t.fd, 0, 0)
	if err != 0 {
		return err
	}
	if ret != 0 {
		return errors.New("bad return from close()")
	}
	return nil
}

func (t *Timer) blockedRead() (uint64, error) {
	var n uint64
	ret, _, err := syscall.Syscall(unix.SYS_READ, t.fd, uintptr(unsafe.Pointer(&n)), 8)
	if err != 0 {
		return n, err
	}
	if ret != 8 {
		return n, errors.New("bad return from read()")
	}
	return n, nil
}
