package timerfd

import (
	"github.com/alecthomas/assert"
	"github.com/gonum/stat"
	"golang.org/x/sys/unix"
	"testing"
	"time"
)

func TestCreateClose(t *testing.T) {
	delay := time.Millisecond * 10
	tv := itimerspec{Interval: unix.NsecToTimespec(delay.Nanoseconds()), Value: unix.NsecToTimespec(delay.Nanoseconds())}
	t.Log(tv)
	tim, err := Create(delay)
	t.Log(tim)
	assert.NoError(t, err)
	err = tim.Close()
	assert.NoError(t, err)
}

func TestTimer(t *testing.T) {
	tim, err := Create(time.Millisecond * 10)
	assert.NoError(t, err)
	t1 := time.Now()
	diffs := make([]float64, 100)
	for i := 0; i < 100; i++ {
		tim.blockedRead()
		t2 := time.Now()
		// t.Log(t2.Sub(t1))
		diffs[i] = t2.Sub(t1).Seconds()
		t1 = t2
	}
	m, s := stat.MeanStdDev(diffs, nil)
	t.Log(m, s)
	err = tim.Close()
	assert.NoError(t, err)
}

func TestTimeTimer(t *testing.T) {
	ticch := time.Tick(time.Millisecond * 10)
	t1 := time.Now()
	// var sum float64
	var cnt = 1
	diffs := make([]float64, 100)
	for _ = range ticch {
		t2 := time.Now()
		// sum += t2.Sub(t1).Seconds()
		// avg := sum / float64(cnt)
		diffs[cnt-1] = t2.Sub(t1).Seconds()
		// t.Log(t2.Sub(t1), avg*1000)
		t1 = t2
		cnt++
		if cnt == 101 {
			break
		}
	}
	m, s := stat.MeanStdDev(diffs, nil)
	t.Log(m, s)
}

func TestFdTimer(t *testing.T) {
	ticch := Tick(time.Millisecond * 10)
	t1 := time.Now()
	// var sum float64
	var cnt = 1
	diffs := make([]float64, 100)
	for _ = range ticch {
		t2 := time.Now()
		// sum += t2.Sub(t1).Seconds()
		// avg := sum / float64(cnt)
		diffs[cnt-1] = t2.Sub(t1).Seconds()
		// t.Log(t2.Sub(t1), avg*1000)
		t1 = t2
		cnt++
		if cnt == 101 {
			break
		}
	}
	m, s := stat.MeanStdDev(diffs, nil)
	t.Log(m, s)
}
